package algorithm;
import  java.util.*;

public class GroupWordsByLength {
	public static ArrayList<ArrayList<String>> groupWordsByLengh(ArrayList<String> strList){
		//Null parameter check
		if(strList == null){
			return null;
		}
		
		
		//Get list length
		int len = strList.size();
		
		//Return null if string length is 0
		if(len ==0){
			return null;
		}
		
		//Mapping word's length with String array
		Map<Integer ,ArrayList<String>> map = new TreeMap<Integer, ArrayList<String>>();
		
		//Iterate all the word in list
		for(String str : strList){
			int strLen = str.length();
			
			//Get word array stored in map by word length
			ArrayList<String> list = map.get(strLen);
			
			//If word's length is not found in map key,insert the key with value
			if(list == null){
				ArrayList<String> array = new ArrayList<String>();
				array.add(str);
				map.put(strLen, array);
			}else{
				//Word's length is found in map, so add word to same word length array
				list.add(str);
			}
		}
		
		ArrayList<ArrayList<String>> groupList = new ArrayList<ArrayList<String>>();
		//Compose group list from treemap
		for(Integer key : map.keySet()){
			groupList.add( map.get(key));
		}
		
		return groupList;
	}
	
    public static void main(String args[]) { 
    	String [] strArray = {"Hello","World","cup","dog","cat","tank","a","b",""};  
    	
    	ArrayList<String> strList = new ArrayList<String>(Arrays.asList(strArray));
    	ArrayList<ArrayList<String>> groupList = GroupWordsByLength.groupWordsByLengh(strList);
    	System.out.println("Input   is : " + strList);
    	//Output is in order.
    	System.out.println("Outpout is : " + groupList);
    } 
}

