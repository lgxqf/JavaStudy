/*Functionality:
 	Permutate a word and output all it permutation
 	Word can't contain space character
 	abc:   abc  acb  cab cba  bac bca
 	abc:   abc  acb  bac bca  cab cba
 */

/*Cases:
 * 
Invalid:	
	Null word
	Word is ""
	
	
Valid:		
	Bound��
		Word has only 1 character
		
	InBound��
		Word has more than 1 character

Optimize:
	

-----------Cases Review Result

Added:

Removed:

 **/

package algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class WordPermutation {
	public static void main(String args[]){
		String wordList[] ={
				null,
				"",
				"a",
				"ABCD",
		};
		
		for(String s : wordList){
			System.out.println("===============Test Boundary============================");
			System.out.println("String :" + s);
			WordPermutation wp = new WordPermutation();
			wp.Permutate(s);
		}		
	}
	
	private char []m_word;
	private ArrayList<String> m_aryList;
	private int m_len = 0;
	
	public WordPermutation(){
	}
	
	public void Permutate(String word){
		if(null == word || word.length() == 0){
			System.out.println("ERROR: Empty or NULL string");
			return;
		}
			
		m_aryList = new ArrayList<String>();		
		m_word = word.toCharArray();
		m_len = m_word.length;
		
		Permutate(m_len);		
		
		//Collections.sort(m_aryList);
		System.out.println(m_aryList);		
		System.out.println(m_aryList.size());
	}
	
	private void Permutate(int len){		
		if(1 == len){
			System.out.println( m_word);			//System.out.print(String.valueOf(++num) + " ");
			m_aryList.add(String.valueOf(m_word));
			return;			
		}else{			
			int i = 0;
			
			while(i < len ){				
				char temp = m_word[m_len - len ];
				m_word[m_len - len ] = m_word[m_len - len +i];
				m_word[m_len - len +i] = temp;		
				
				Permutate(len -1);
				//System.out.println("===============" + String.valueOf(m_word));
				
				temp = m_word[m_len - len ];
				m_word[m_len - len ] = m_word[m_len - len +i];
				m_word[m_len - len +i] = temp;
				//System.out.println("++++++++++++++++" + String.valueOf(m_word));

				i++;
			}			
		}
	}
		
}


