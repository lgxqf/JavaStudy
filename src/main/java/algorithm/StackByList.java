package algorithm;

class Element{
	Element next;
	int data;
	
	public Element(int val){
		data = val;
	}
};

public class StackByList {
	
	public static void main(String args[]){
		
		StackByList sl = new StackByList();
		System.out.println("==============push===================");
		for(int i =1; i <=3; i++){
			Element temp = new Element(i);
			sl.push(temp);	
			System.out.println(temp.data);
		}
		
		Element temp = null;
		
		System.out.println("==============pop====================");		
		for(int i = 0; i< 6; i++){
			temp = sl.pop();
			if(null != temp){
				System.out.println(temp.data);	
			}else{
				System.out.println("NULL");
			}
			
		}

	}
	
	private Element m_top;
		
	public StackByList(){

	}
	
	public void push(Element elem){
		elem.next = m_top;
		m_top = elem;
		}
	
	public Element pop(){
		Element temp = null;
		
		if(isEmpty())
			return temp;
		
		temp = m_top;
		m_top = m_top.next;
				
		return temp;
	}
	
	public boolean isEmpty(){
		return m_top == null;
	}
}
