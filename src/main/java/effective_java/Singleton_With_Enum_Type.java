package effective_java;

public class Singleton_With_Enum_Type {
	
	public static void main(String[] str){
	
		Movie inst = Movie.getInstance();
		
		EnumSingleTon enumSingleton = EnumSingleTon.INSTANCE;
		enumSingleton.test();
	}
}

class Movie{
	
	private final static Movie inst = new Movie();
	private Movie(){}
	
	public static Movie getInstance(){
		return inst;
	}
}

//Enum Singleton
enum EnumSingleTon{
	
	INSTANCE;
	
	public void test(){
		System.out.println("Enum Singleton!");
	}
}

