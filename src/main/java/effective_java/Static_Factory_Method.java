package effective_java;

public class Static_Factory_Method {
	
	public static void main(String[] args){
		Cat a = Cat.newInstance();
	}
}

class Cat{
	
	private Cat(){}
	
	public static Cat newInstance(){
		return new Cat();
	}
}
