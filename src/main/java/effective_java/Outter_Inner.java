package effective_java;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */

class a{}
class b extends a{}
class c extends b{}

public class Outter_Inner {
    private int value;
    String s;
    EnumOperation plus = EnumOperation.PLUS;

    public int getValue(){
        return value;
    }

    //Inner class
    class Inner {
        public void addValue() {
            value = value + 1;
        }
    }

    //Static inner class
    static class static_inner{
        public static void main(String[] args){
            System.out.print("Staic inner class main method");
        }
    }

    public enum  APPLE{A,B}
    public enum ORANGE{C,D}

    public void M(a[]  c){}
    public void MM(){
        b []bb = new b[]{};
        M(bb);
    }

    public void testExtend(List<?extends b> c){}
    public void testSuper(List<?super b> c){}

    public void test(){
        List<a> aA= new ArrayList<a>();
        List<b> bB = new ArrayList<>();
        List<c> cC = new ArrayList<>();

        testExtend(bB);
        testExtend(cC);

        testSuper(bB);
        testSuper(aA);
    }
}

