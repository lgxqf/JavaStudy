package effective_java;

public class Use_Builder_If_Too_Much_Constructor_Parameter {

	public static void main(String[] str)
	{
		Car myCar = new Car.CarBuilder(1, "Manual").setAutoStart(true).setDoorNumber(5).createCar();
		System.out.println(myCar.toString());
	}
}

class Car{
	
	public static class  CarBuilder{
		
		private float price = 0;
		private String mode = "Auto";
		
		private boolean autoStart = false;
		private int doorNum = 4;
		
		public CarBuilder(float price, String mode){
			this.price = price;
			this.mode = mode;
		}
		
		public CarBuilder setAutoStart(boolean autoStart){
			this.autoStart = autoStart;
			return this;
		}
		
		public CarBuilder setDoorNumber(int doorNumber ){
			this.doorNum = doorNumber;
			return this;
		}
		
		public Car createCar(){
			return new Car(this);
		}
	}//end CarBuilder
	
	private float price = 0;
	private String mode = "Auto";
	
	private boolean autoStart = false;
	private int doorNum = 4;
	
	private Car(CarBuilder builder){
		price = builder.price;
		mode = builder.mode;
		autoStart = builder.autoStart;
		doorNum = builder.doorNum;
	}
}