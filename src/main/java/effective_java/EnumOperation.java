package effective_java;

/**
 * Created by justin on 13/11/2016.
 */
public enum EnumOperation {
    PLUS,
    MINUS,
    TIMES,
    DIVIDE
}
