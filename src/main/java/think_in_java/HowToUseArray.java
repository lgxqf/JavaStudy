package think_in_java;

public class HowToUseArray {
	String HowToUseArray(){
		return null;
	}
	
	public static void main(String[] args)
	{
		HowToUseArray a = new HowToUseArray();
		a.HowToUseArray();
		
		//Correct usage
		int [] ary;
		
		int [] aryA = {1,2};
		
		int [] aryB = new int[2];
		aryB[0] = 1;
		aryB[1] = 2;
		
		int [] aryC = new int[]{1,2};
		
		//Wrong usage
		
		//int [] aryD = new int[2]{1,2};
		
		//int aryE[4];
		
		
	}
}
