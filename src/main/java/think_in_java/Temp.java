package think_in_java;
import static common.Print.*;

import java.util.ArrayList;

import common.*;

class Go {
	String name = this.getClass().getName();
	static String s1 = "run";
	static String s2, s3;
	static {
		s2 = "drive car";
		s3 = "fly plane";
		print("s2 & s3 initialized");
	}
	static void how() {
		print("abc");
	}
	public void Test(Object... obj) { 
		show(obj);
	}	
	
	public void show(Object... obj){
		if(null == obj){
			System.out.println("NULL");
			return;
		}
		
		for(Object object : obj){
			Integer in = (Integer)object;
			System.out.println(in);
		}
	}
	
	public void d(){}
}

class Pair<T>{
}

class DerPair <T> extends Pair<T>{
}

class mysuper{}
class Base extends mysuper{}
class Deriver extends Base{}
class C{
	static  <T extends Base> T test( T a){
		return a;
	}
}

public class Temp extends Go{
	public void d(){}
	public static void main(String[] args) {
		String a = "0";
		System.out.println(a == "0");
		//C.test(new mysuper());
		Base d = C.test(new Base());
		Base dd = 		C.test(new Deriver());
	}
}