package utility;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;


public class RenameFile_MultiThread extends Thread{

	public static void main(String args[]){
	
		RenameFile_MultiThread  thread = new RenameFile_MultiThread ("E:\\Personal\\Pic\\Irdeto", true);
	}
	
	private final String  m_path;
	private final boolean m_subFolder;
	
	public RenameFile_MultiThread(String path, boolean includeSubFolder){
		m_path = path;
		m_subFolder = includeSubFolder;
		start();
	}
	
	public void run(){
		action(m_path);
	}
	
	public boolean action(String dirName){
		
		if(null == dirName){
			System.out.println("Directory name is NULL !");
			return false;
		}
		
		File directory = new File(dirName);
		
		//Check if path exists 
		if(!directory.exists()){	
			System.out.println("No such Directory : " + dirName);
			return false;
		}
		
		//Check if path is writable
		if(!directory.canWrite()){
			System.out.println("Directory : " + dirName + " is read only");
			return false;
		}
		
		//Check if path is directory rather than file
		if(directory.isFile()){
			System.out.println("Directory : " + dirName + " is file rather than directory");
			return false;
		}
        
		//Folder count of current directory
        int folderCount = 0;
        //File count of current directory
        int fileCount = 0;
        
		//Traverse sub folders
		for(File dir : directory.listFiles()){
			if(dir.isDirectory() && m_subFolder){
				Thread thread = new RenameFile_MultiThread(dir.toString(),m_subFolder);				
				folderCount ++;
				//System.out.println(dir.toString());
			}else{
				fileCount++;
			}
		}

        FilenameFilter imageFilter = new FilenameFilter(){
        	public boolean accept(File dir, String name){
        		return name.toLowerCase().endsWith(".jpg");
        	}
        };
        
		//Check if there is any image file under path
        File fileList[] = directory.listFiles(imageFilter);
        if(0 == fileList.length){
			System.out.print("!!!No image file found under directory " + dirName);
			if( folderCount > 0 ){
				System.out.println( " , but it has " + String.valueOf(folderCount) + " folders");
			}
			
			return false;
        }

       	Comparator<ImagePack> cmpLong = new Comparator<ImagePack>(){
        	@Override
        	public int compare(ImagePack a, ImagePack b){
        		return a.m_time <b.m_time ? -1 : (a.m_time==b.m_time ? 0 : 1) ;
        	}
        };
        
        ArrayList<ImagePack> imagePackList= new ArrayList<ImagePack>(); 
        //Get image file list and store it into ArrayList.
        for(File imgFile : fileList){
        	imagePackList.add( new ImagePack( imgFile , imgFile.lastModified() ) );
        }
        //Sort the ArrayList according to last modified time of file
        Collections.sort(imagePackList,cmpLong );
        
        //Count of renamed failure images  
        int failCount = 0;
        File file = null;
        String picName = null;
        String lastName = null;
        long dupCount = 0;        
    	final String timePattern = "yyyy-MM-dd";
    	SimpleDateFormat dateFormat = new SimpleDateFormat(timePattern);
    	
        Iterator<ImagePack> it = imagePackList.iterator();
        ImagePack imgpack = null;
        
        //Rename each image file        
        while(it.hasNext()){
        	imgpack = it.next();
        	file = imgpack.m_file;
        	Long time = imgpack.m_time;
        	picName = dateFormat.format(new Date(time));
        	
        	if(picName.equals(lastName)){
        		dupCount ++;
        	}else{
        		lastName = picName;
        		dupCount = 0;
        	}
        	
        	//Build picture new name according its last modified time.
        	StringBuilder name = new StringBuilder(dirName);
        	name.append("\\");
        	name.append(picName);
        	if(0 != dupCount){
        		name.append("(");
        		name.append(dupCount);
        		name.append(")");
        	}
        	name.append(".jpg");
        	
        	if(file.renameTo(new File(name.toString())))
        	{
        		
        	}else{
        		failCount ++;
        		System.out.println("Fail to rename file " + file.toString());
        	}
        }
        
        String separator = " | ";
        StringBuilder output = new StringBuilder(dirName.toString());
        
        output.append(" : has ");
        output.append(String.valueOf(fileCount));        			output.append(" files");		     output.append(separator);
        output.append(String.valueOf(imagePackList.size()));        output.append(" image files");       output.append(separator);
        output.append(String.valueOf(failCount)); 			        output.append(" failed");      		 output.append(separator);
        output.append(String.valueOf(folderCount));			        output.append(" folders"); 		     output.append(separator);              
		
        System.out.println(output);
        
		return true;
	}
}
