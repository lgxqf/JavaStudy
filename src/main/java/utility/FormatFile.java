package utility;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;

//Format the format of Makefile. Add necessary Tab to improve the readability of the Makefile
public class FormatFile {
    public static void main(String[] args){
        final String fileName = "D:\\Makefile";
        
        FormatFile file = new FormatFile(fileName);
        file.formatFile();
    }

    private int m_tabCount = 0;
    private File m_inputFile=null;
    private final String[] m_keywordList={"else","ifdef","ifndef","ifeq","ifneq","endif",};
    
    public FormatFile(String fileName){
        m_inputFile = new File(fileName);
    }
    
    private boolean checkFileValidity(){
        return true;
    }
    
    private void writeLineToFile(BufferedWriter writer,String line) throws IOException{
        for(int i = 0; i < m_tabCount; i ++){
            writer.write("\t");
        }
        writer.write(line);
        writer.write("\n");
    }
    
    private int getKeyIndex(String line){        
        int index = 0;
        int keyIndex = 0;
        for(String str : m_keywordList){
            index = line.indexOf(str);
            if(-1 != index){
                break;
            }
            keyIndex ++;
        }
        //-1 means Line contains no keyword
        return keyIndex;
    }
    
    private void processLine(BufferedWriter writer,String line)throws IOException{
        int index = getKeyIndex(line);
        int preTabCount = 0;
        int postTabCount = 0;
        
        switch(index){
            case 0://else && else if            
                preTabCount = -1;
                postTabCount = 1;
                break;
            case 1://if
            case 2:
            case 3:
            case 4:
                postTabCount = 1;
                break;
            case 5://endif
                preTabCount = -1;
                break;
            default:
                break;
        }
        
        m_tabCount += preTabCount;
        writeLineToFile(writer,line);
        m_tabCount += postTabCount;
    }
    
    public boolean formatFile(){
        boolean ret = false;
        
        ret = checkFileValidity();        
        if(!ret){
            return ret;
        }
        
        final String outputFileName = m_inputFile.getAbsolutePath() + ".txt";
        
        try{
            BufferedWriter outputFile   = new BufferedWriter(new FileWriter(outputFileName));
            LineNumberReader lineReader = new LineNumberReader(new FileReader(m_inputFile));
            String currentLine = lineReader.readLine();

            while(null != currentLine){
                processLine(outputFile,currentLine);
                currentLine = lineReader.readLine();
            }
            
            lineReader.close();
            outputFile.flush();
            outputFile.close();
            System.out.println("Succeed in formating file :" + m_inputFile.getAbsolutePath() + " to file: " + outputFileName);
        }catch(Exception e){
            System.out.println();
            System.out.println("Exception: " + e.getClass().getName() + " "+ e.getMessage());
            return false;
        }
        
        return ret;
    }
}
