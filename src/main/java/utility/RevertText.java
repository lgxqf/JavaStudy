/*Functionality:
 	A sentence is separated by space character 
 	Revert this sentence as this example: 	"Hello World!"--->"!World Hello"
 */

/*Cases:
 * 
Invalid:	
	Sentence validity check
		Empty String
		Null String
		//String is too long that String variable can not afford such a big string.
	
Valid:		
	Bound： Extreme sentence content cases 
		Sentence contains only one space character
		Sentence contains more than one space character, but contains space character only 
	
		-->Sentence contains one character only, and the character have no space
		++>Sentence have more than one word, and one of its words has only one character
		++>Sentence have more than one word, and all of its words has only one character
		++>Sentence have more than one word, and none of its words has only one character	
			Sentence contains one character only, and the character have leading space, but no trailing space
			Sentence contains one character only, and the character have trailing space, but no leading space
			Sentence contains one character only, and the character have both trailing space and leading space
	
	InBound：
		Sentence contains one word only, and the word have neither trailing space nor leading space
		Sentence have more than one word, and the sentence have leading space, but no trailing space
		Sentence have more than one word, and the sentence have trailing space, but no leading space
		Sentence have more than one word, and the word have both trailing space and leading space
		Check how it deal with space key. "  SPACE "
-----------Cases Review Result

Added:
	Sentence have more than one word, and one of its words has only one character
	Sentence have more than one word, and none of its words has only one character
	Sentence have more than one word, and all of its words has only one character
	"  SPACE " to check how it deals with space key, result could be " SPACE  " or "  SPACE " 
	
Removed:
	Sentence contains one character only, and the character have no space 

程序中的第一个for 和 while语句
//F !W	//!F W
!F !W
任意单个字符 

W1~W3指While语句的前两个While和其中的For
字符串由任意两个及以上字符组成
		！W1 字符串仅含空格 或 当前字符不是空格   			W1 当前字符是空格
		！W2 字符串没有空格 或 当前字符是空格				W2 当前字符不是空格
		! F3 含有单个字母的单词							F3 含两个及以上字母的单词

！W1 仅含空格 "   "

！W2 没有空格,！W1 当前字符不是空格
	!W1 F3	"a"
	!W1!F3	"ab"
	
	W1 W2 F3	" a"
	W1 W2 !F3  " ab"
	
！W1 当前字符不是空格
！W2 前字符是空格	
	!W1 !W2 !F3	"a "
	!W1 !W2 F3	"ab "
 **/
package utility;

public class RevertText {
	
	public static void main(String[] args){
		
		String textData[] = { 

				//null,
				"",
				//
				" ",
				"   ",
				//
				"A",//-->equals to " "
				" A",
				"A ",
				" A ",
				"AB",//++>
				//
				" hello my world ",
				"hello my world",
				"  hello my world ",
				"hello my world  ",	
				//
				" hello my world ！ ",//++>
				"hello my world !",//++>
				"  hello my world !",//++>
				"hello my world ! ",//++>
				//
				"A A",//++>
				" A A ",//++>
				"A A ",//++>
				" A A",//++>
				//
				" Space  ",
		};
		
		RevertText revert = new RevertText();
		String result = null;
		
		for(String str : textData)
		{
			System.out.println("Oringal: " + str.replace(' ','^' ));
			
			result = revert.action(str.toCharArray());
			
			if(null != result){
				System.out.println("Result : " + result.replace(' ', '^'));
			}
			
			System.out.println("=======================================");
		}
		
	}
	
	public String action(char [] str){
		
		if(null == str){
			System.out.println("Sentence is NULL");
			return null;
		}
		
		String result = null;		
		int strLen = str.length;
		
		if(0 == strLen){
			System.out.println("Sentence is empty");
			return null;
		}
		 
		//Swap each character in the string
		char temp;
		int index = 0;
		
		for(index = 0; index < (strLen /2) ; index ++ ){
			temp = str[index];
			str[index] = str[strLen - 1 - index];
			str[strLen -1 - index]  = temp;
		}
		
		index = 0;
		
		//Swap the character in each word until reach the end of string
		//How to find a word:(1)Before reaching the end of String,find the first non space key to locate the head of word
		//					 (2)then,before reaching the end of String, find the first space key to locate the tail of the word
		int wordHead = 0;//Location of the head of the found string
		int wordTail = 0;//Location of the tail of the found string
		int tempIndex = 0;
		int wordLen = 0;
		final char spaceKey = ' ';
		
		while(index < strLen -1){// "strLen -1" is better than "strLen", because if the sentence contains only one character, skip "while" loop
								 // Also, when the last word of sentence have only one character, we don't need to deal with it.
			//Find the head of word 
			//Before reaching the end of String,find the first non space key to locate the head of word
			while(index < strLen && str[wordHead] == spaceKey){
				wordHead ++;
				wordTail ++; 
				index ++;
			}
			
			//Find the tail of word
			//Before reaching the end of String, find the first space key to locate the tail of the word
			while(index < strLen && str[wordTail] != spaceKey){
				wordTail++;//Points to the first space key after the found word
				index ++;
			}

			//Make it point to the word's latest letter rather than the first space key after the found word.
			//In case index == strLen caused by the last character of String is not Space key
			//To Avoid buffer overflow for str[wordTail], make wordTail equal to the wordTail -1
			wordTail --;
			
			wordLen = wordTail - wordHead + 1;//Actual length of the found word
			
			//Swap the character of the found word if it has more than one character wordLen> 1
			//if( wordLen > 1)
			//{
			
			for(tempIndex = 0;tempIndex < wordLen/2;tempIndex ++){
				temp = str[wordHead + tempIndex];
				str[wordHead + tempIndex] = str[wordTail - tempIndex];
				str[wordTail - tempIndex] = temp;
			}
			//}
			
			//Set wordTail and wordHead point to the same location to make them useful in next loop
			wordTail ++;//Points to the first space key after the found word
			wordHead = wordTail;
		}
		
		result = String.valueOf(str);
		
		return result;
	}
	
}
