package utility;
import java.io.*;
/*Test Case
 *	
Empty line:
	Line with "CR LF" character or only Blank character
	
File check
	File doesn't exist
	File is not writable and can't be opened
	File is empty
	File is a folder rather than a file
	//Long file name	//File name with unusual symbol

File content
	--Bound: 
	No thing to be modified
	File Content won't be modified it already matched the expected result.
	Only empty line
	Only one line and one not empty line
	
	--Bound: 
	Everything needs to be modified
	More than one line and no empty Line
	Three non-empty line,each followed by empty line count increased by 1
	
	--InBound:
	Some content needs to be modified while the rest needn't.
	Some content match the expected result, the rest doesn't.
	
Bracket : NOT Implemented
	Head and tail both have bracket
	Head no bracket while tail has
	Tail no bracket while head has
	Both head and tail have not bracket

 * */
 
public class ParseText {
	
	private final String fileName;
	private File m_File = null;
	private final String m_tempFileName;

	public static void main(String[] args){
		
		String fileName = "C:\\Documents and Settings\\Kim\\����\\new.txt";
		ParseText parseText = new ParseText(fileName);

		if(parseText.checkFile()){
			parseText.parseFile();
		}
	}
	
	public ParseText(String txtFileName){
		fileName = txtFileName;
		m_tempFileName = m_File +".tmp";
	}
	
	public boolean checkFile(){
		
		m_File = new File(fileName);
		
		//Check if the file exists
		if(!m_File.exists()){
			System.out.println("Error: \"" + fileName + "\" does not exist");
			return false;
		}
		
		//Check if it is a file
		if(!m_File.isFile()){
			System.out.println("Error: \"" + fileName + "\" is not a file");
			return false;
		}

		//If if the file is accessible and writable
		if(!m_File.canRead()){
			System.out.println("Error: \"" + fileName + "\" is not readable");
			return false;
		}
		
		//Check if the file is empty
		if(0 == m_File.length()){
			System.out.println("Error: \"" + fileName + "\" is empty");
			return false;
		}

		System.out.println("All File check passed!");
		return true;
	}
	
	public boolean parseFile(){
		
		if(null == m_File){
			return false;
		}
		
		File tempFile = new File(m_tempFileName);
		String currentLine = null;
				
		try{
		
			LineNumberReader lineReader = new LineNumberReader(new FileReader(m_File));
			String tempString = lineReader.readLine().trim();//Read first line
			
			currentLine = lineReader.readLine();//Read second line
			
			//If file have one line just return else create new output file
			//null means it reaches the end of file while reading the second line
			if(null == currentLine){
				System.out.println("File:" + m_File + " has only one line.");
				return true;
			}
			
			//Create temporary file for output file
			if(false == tempFile.createNewFile() ){
				System.out.println("Fail to create temp file " +  m_tempFileName + " for output file");
				return false;
			}
			
			//Write first line to output file
			BufferedWriter outputFile = new BufferedWriter(new FileWriter(tempFile));
			
			//Write first line to output file if it it not empty
			if(!tempString.isEmpty()){
				
				outputFile.write(tempString);
				outputFile.newLine();
				outputFile.newLine();
				System.out.println("Write first line of input file to output file ");
			}
			
			//<While>	Do following action until all file content is parsed
				//Read one line, if it is not empty, write it to output file.
			//</While>
			while(null != currentLine){
				
				if(!currentLine.trim().isEmpty()){
					
					outputFile.write(currentLine);
					outputFile.newLine();
					outputFile.newLine();
					System.out.println("Write content of line no: " + lineReader.getLineNumber() + " of input file to output file ");
					
				}
				
				currentLine = lineReader.readLine();
			}
			
			outputFile.flush();
			outputFile.close();
			lineReader.close();
			System.out.println("Finish writting to output file");
			
			//Delete input file and rename output file to input file
			m_File.delete();
			tempFile.renameTo(m_File);
			System.out.println("Rename output file name to input file name");
			
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		return true;
	}

}
