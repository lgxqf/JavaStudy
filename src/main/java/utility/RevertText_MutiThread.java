package utility;

import java.util.ArrayList;

/*Functionality:
 	Multiple Thread version of RevertText
	A sentence is separated by space character 
	Revert this sentence as this example: 	"Hello World!"--->"!World Hello"
*/

class RevertWord extends Thread
{
	private static int threadCount = 0;
	private char m_str[];
	private int m_Head;
	private int m_Tail;
	
	private synchronized void threadCountPlus(){
		threadCount++;
		System.out.println("Thread " + String.valueOf(threadCount) + " Starts" );
	}
	
	public RevertWord(char str[],int wordHead, int wordTail){
		m_str = str;
		m_Head = wordHead;
		m_Tail = wordTail;
		
		threadCountPlus();		
		start();
	}
	
	public void run(){
		
		int index = 0;
		int wordLen = m_Tail - m_Head + 1;
		char temp;
		
		for(index = 0; index < wordLen/2;index ++){
			temp = m_str[m_Head + index];
			m_str[m_Head + index] = m_str[m_Tail - index];
			m_str[m_Tail - index] = temp;
		}
	}
}

public class RevertText_MutiThread {

public static void main(String[] args){
	
	RevertText_MutiThread revert = new RevertText_MutiThread();
	
	
	String textData[] = { 
			" You are my best friends !  ",
			"Thrown when a thread is waiting, sleeping, or otherwise occupied, and the thread is interrupted, either before or during the activity. Occasionally a method may wish to test whether the current thread has been interrupted, and if so, to immediately throw this exception. The following code can be used to achieve this ",
	};

	String result = null;	
	for(String str : textData){
		System.out.println("Oringal: " + str.replace(' ','^' ));
		
		result = revert.action(str.toCharArray());
		if(null != result){
			System.out.println("Result : " + result.replace(' ', '^'));
		}
		
		System.out.println("=======================================");
	}
}

public String action(char [] str){
	
	if(null == str){
		System.out.println("Sentence is NULL");
		return null;
	}
	
	String result = null;		
	int strLen = str.length;
	
	if(0 == strLen){
		System.out.println("Sentence is empty");
		return null;
	}
	 
	//Swap each character in the string
	char temp;
	int index = 0;
	
	for(index = 0; index < (strLen /2) ; index ++ ){
		temp = str[index];
		str[index] = str[strLen - 1 - index];
		str[strLen -1 - index]  = temp;
	}
	
	index = 0;
	
	//Swap the character in each word until reach the end of string
	//How to find a word:(1)Before reaching the end of String,find the first non space key to locate the head of word
	//					 (2)then,before reaching the end of String, find the first space key to locate the tail of the word
	int wordHead = 0;//Location of the head of the found string
	int wordTail = 0;//Location of the tail of the found string
	int wordLen = 0;
	final char spaceKey = ' ';
	
	ArrayList<Thread> wordThreadList = new ArrayList<Thread>();
	
	while(index < strLen -1){// "strLen -1" is better than "strLen", because if the sentence contains only one character, skip "while" loop
							 // Also, when the last word of sentence have only one character, we don't need to deal with it.
		//Find the head of word 
		//Before reaching the end of String,find the first non space key to locate the head of word
		while(index < strLen && str[wordHead] == spaceKey){
			wordHead ++;
			wordTail ++; 
			index ++;
		}
		
		//Find the tail of word
		//Before reaching the end of String, find the first space key to locate the tail of the word
		while(index < strLen && str[wordTail] != spaceKey){
			wordTail++;//Points to the first space key after the found word
			index ++;
		}

		//Make it point to the word's latest letter rather than the first space key after the found word.
		//In case index == strLen caused by the last character of String is not Space key
		//To Avoid buffer overflow for str[wordTail], make wordTail equal to the wordTail -1
		wordTail --;
		wordLen = wordTail - wordHead + 1;//Actual length of the found word
		
		if(0 < wordLen/2){
			Thread  wordThread = new RevertWord(str,wordHead,wordTail);
			wordThreadList.add(wordThread);
		}		
		
		//Set wordTail and wordHead point to the same location to make them useful in next loop
		wordTail ++;//Points to the first space key after the found word
		wordHead = wordTail;
	}
	
	for(Thread thread: wordThreadList){
		try{
			thread.join();	
		}catch(InterruptedException e){
			System.out.println(e);
		}
	}
	
	result = String.valueOf(str);
	
	return result;
}

}
